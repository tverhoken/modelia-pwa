import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from "../services/user/user.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor (
    private userService: UserService,
    private router: Router,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (route.url.length === 0) {
      return this.userService.getUser() ? this.router.createUrlTree(['/accueil']) : this.router.createUrlTree(['/login'])
    } else if (route.url.length === 1 && route.url[0].path === 'login') {
      return this.userService.isGuest() || !this.userService.getUser() ? true : this.router.createUrlTree(['/accueil']);
    } else {
      return this.userService.getUser() ? true : this.router.createUrlTree(['/login']);
    }
  }
  
}
