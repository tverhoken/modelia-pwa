import { TestBed } from '@angular/core/testing';

import { AmbientLightSensorService } from './ambient-light-sensor.service';

describe('AmbientLightSensorService', () => {
  let service: AmbientLightSensorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AmbientLightSensorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
