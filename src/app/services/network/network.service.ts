import {Injectable, OnDestroy} from '@angular/core';
import {fromEvent, merge, Observable, Observer, Subscription} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  online$: Observable<Event> = new Observable<Event>();
  offline$: Observable<Event> = new Observable<Event>();

  constructor() {
    this.online$ = fromEvent(window, 'online');
    this.offline$ = fromEvent(window, 'offline');
  }

  monitor(): Observable<boolean> {
    return merge<boolean>(
      fromEvent(window, 'offline').pipe(map(() => false)),
      fromEvent(window, 'online').pipe(map(() => true)),
      new Observable((sub: Observer<boolean>) => {
        sub.next(navigator.onLine);
        sub.complete();
      }));
  }

}
