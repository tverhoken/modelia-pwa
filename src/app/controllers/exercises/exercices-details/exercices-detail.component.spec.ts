import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercicesDetailComponent } from './exercices-detail.component';

describe('ExercicesDetailComponent', () => {
  let component: ExercicesDetailComponent;
  let fixture: ComponentFixture<ExercicesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExercicesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExercicesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
