import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExercisesComponent } from './exercises.component';
import { ExercisesRoutingModule } from './exercises-routing.module';
import {MatListModule} from '@angular/material/list';
import { ExercicesDetailComponent } from './exercices-details/exercices-detail.component';
import {MatCardModule} from "@angular/material/card";


@NgModule({
  declarations: [
    ExercisesComponent,
    ExercicesDetailComponent
  ],
  imports: [
    CommonModule,
    ExercisesRoutingModule,
    MatListModule,
    MatCardModule
  ]
})
export class ExercisesModule { }
