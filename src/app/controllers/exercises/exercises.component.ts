import {Component, OnInit} from '@angular/core';
import {ExerciseDefinition} from 'src/app/models/Exercise';
import {exerciseDefinitions} from 'src/app/models/Exercise';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss']
})
export class ExercisesComponent implements OnInit {

  exercises = exerciseDefinitions;
  selectedExercise = exerciseDefinitions[0];

  constructor() {
  }

  ngOnInit(): void {
  }

  onSelect(exercise: ExerciseDefinition): void {
    this.selectedExercise = exercise;
  }

}
