import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ExercisesComponent } from './exercises.component';



@NgModule({
  imports: [RouterModule.forChild([{path: '', component: ExercisesComponent}])],
  exports: [RouterModule]
})
export class ExercisesRoutingModule { }
