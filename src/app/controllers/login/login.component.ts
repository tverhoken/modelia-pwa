import {Component, OnInit} from '@angular/core';
import {MonitorComponent} from "../../components/monitor/monitor.component";
import {NetworkService} from "../../services/network/network.service";
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LocalStorageService} from "../../services/local-storage/local-storage.service";
import {ActivityService} from "../../services/activity/activity.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends MonitorComponent {

  loginForm: FormGroup;
  errorMsg: string | null;
  loading = false;

  constructor(
    protected networkService: NetworkService,
    protected userService: UserService,
    protected router: Router,
    private fb: FormBuilder,
    private localStorageService: LocalStorageService,
    private activityService: ActivityService,
    private snackbar: MatSnackBar,
  ) {
    super(networkService);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.initForm();
  }

  initForm(): void {
    this.loginForm = this.fb.group({
      email: this.fb.control('mikolai.krol@gmail.com', [Validators.required, Validators.email]),
      password: this.fb.control('krol', [Validators.required]),
    })
  }

  login(method: string): void {
    if (method === 'guest') {
      this.userService.loginAsGuest();
    } else {
      this.loading = true;
      this.errorMsg = null;
      this.userService.login(this.loginForm.value)
        .subscribe(resp => {
            this.userService.setUser(resp["user"]);
            this.userService.setIsGuest(false);
            this.localStorageService.set('token', resp["accessToken"]);
            const guestActivities = this.localStorageService.get(LocalStorageService.KEYS.activities);
            if (guestActivities) {
              if (confirm('Des activités sont stockées localement, voulez vous les synchroniser avec votre compte ?')) {
                guestActivities.forEach(a => {
                  delete a.creator;
                  a.creatorId = resp["user"]["_id"]
                });
                this.activityService.create(guestActivities).subscribe(() => {
                    this.localStorageService.remove(LocalStorageService.KEYS.activities);
                  },
                  () => this.snackbar.open('Les activités stockées localement n\'ont pas pu être synchronisées.', 'Fermer', {duration: 5000}));
              } else {
                this.localStorageService.remove(LocalStorageService.KEYS.activities);
              }
            }
            this.loading = false;
            this.router.navigate(['/accueil']);
          },
          error => {
            this.loading = false;
            this.errorMsg = 'Adresse mail ou mot de passe incorrect, veuillez réessayer';
          });
    }
  }

}
