import {Component, Input, OnInit} from '@angular/core';
import {Activity} from "../../models/Activity";

@Component({
  selector: 'app-activity-summary',
  templateUrl: './activity-summary.component.html',
  styleUrls: ['./activity-summary.component.scss']
})
export class ActivitySummaryComponent implements OnInit {

  @Input() activity: Activity;

  constructor() {
  }

  ngOnInit(): void {
  }

  formatDate(date) {
    return new Date(date).toLocaleString()
  }

  findBestSerie(series) {
    const vols = series?.map(s => parseFloat(s.kg) * parseInt(s.reps));
    let best = 0;
    let bestIndex = 0
    vols.forEach((v, i) => v > best ? bestIndex = i : null);

    return `Meilleure série : ${series[bestIndex]?.reps} x ${series[bestIndex]?.kg}kg`;
  }

}
