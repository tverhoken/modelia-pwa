import User from "./User";
import {Exercise} from "./Exercise";

export class Activity {
  type: 'model' | 'training';
  creator: User;
  creatorId: string;
  id?: string;
  title: string;
  description: string;
  categories?: ActivityCategory[];
  content: Exercise[];
  creationDate: Date
  duration?: number;
  totalWeight?: number;
  certified: boolean

  constructor(type: 'model' | 'training', creator: User) {
    this.type = type;
    this.title = type === 'model' ? 'Nouveau modèle' : 'Nouvelle séance';
    this.description = '';
    this.creator = creator;
    this.creatorId = creator.id;
    this.content = [];
    this.creationDate = new Date();
    this.certified = false;
  }
}

export interface ActivityCategory {
  id: number,
  name: string,
}

export const activityCategories: ActivityCategory[] = [
  {id: 0, name: 'Push'},
  {id: 1, name: 'Pull'},
  {id: 2, name: 'Leg'},
  {id: 3, name: 'Bras'},
  {id: 4, name: 'Pecs'},
  {id: 5, name: 'Dos'},
  {id: 6, name: 'Cardio'},
];
