# Modelia - PWA

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.9.

## Run project

To run this project, run these commands :
- `npm install`
- `ng build`
- `http-server -p 8080 -c-1 dist/modelia-pwa`  

The application will now be available at [localhost](http://127.0.0.1:8080).

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
